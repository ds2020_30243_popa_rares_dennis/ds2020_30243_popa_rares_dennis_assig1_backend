FROM maven:3.6.3-jdk-11 AS builder

COPY ./src/ /root/src
COPY ./pom.xml /root/
COPY ./checkstyle.xml /root/
WORKDIR /root
RUN mvn package
RUN java -Djarmode=layertools -jar /root/target/Assignment_1_Backend-0.0.1-SNAPSHOT.jar list
RUN java -Djarmode=layertools -jar /root/target/Assignment_1_Backend-0.0.1-SNAPSHOT.jar extract
RUN ls -l /root

FROM openjdk:11.0.6-jre

ENV DB_IP=ec2-54-170-123-247.eu-west-1.compute.amazonaws.com
ENV DB_PORT=5432
ENV DB_USER=nndormzgmgpbxz
ENV DB_PASSWORD=088786f7c35ebe21bc369e905ee06bbf053e26c7ca5743857a75d32a6cb9fca1
ENV DB_DBNAME=d6cpv46j6gi6ar


COPY --from=builder /root/dependencies/ ./
COPY --from=builder /root/snapshot-dependencies/ ./

RUN sleep 10
COPY --from=builder /root/spring-boot-loader/ ./
COPY --from=builder /root/application/ ./
ENTRYPOINT ["java", "org.springframework.boot.loader.JarLauncher","-XX:+UseContainerSupport -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -XX:MaxRAMFraction=1 -Xms512m -Xmx512m -XX:+UseG1GC -XX:+UseSerialGC -Xss512k -XX:MaxRAM=72m"]