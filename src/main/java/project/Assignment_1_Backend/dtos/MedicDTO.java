package project.Assignment_1_Backend.dtos;

import javax.validation.constraints.NotNull;
import java.io.Serializable;


public class MedicDTO  implements Serializable {

    private Long id;
    @NotNull
    private UserDTO user;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }
}
