package project.Assignment_1_Backend.dtos;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

public class CaregiverDTO implements Serializable {

    private Long id;
    @NotNull
    private List<PatientDTO> patients;
    @NotNull
    private UserDTO user;

    public CaregiverDTO() {
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public List<PatientDTO> getPatients() {
        return patients;
    }

    public void setPatients(List<PatientDTO> patients) {
        this.patients = patients;
    }
}
