package project.Assignment_1_Backend.controllers.handlers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import project.Assignment_1_Backend.dtos.UserDTO;
import project.Assignment_1_Backend.services.UserService;

@RestController
@CrossOrigin
@RequestMapping(value = "/user")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService){
        this.userService = userService;
    }

    @GetMapping(value = "/checkLogin/{email}/{password}")
    public ResponseEntity<UserDTO> checkLogin(@PathVariable(name = "email") String email,
                                              @PathVariable(name = "password") String password){
        UserDTO userDTO = userService.findUserByEmail(email);
        if(userDTO != null) {
            Boolean ret = new BCryptPasswordEncoder().matches(password, userDTO.getPassword());
            if(ret == true)
                return new ResponseEntity<>(userDTO, HttpStatus.OK);
            else return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }else return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

}
