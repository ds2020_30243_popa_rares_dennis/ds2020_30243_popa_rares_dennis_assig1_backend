package project.Assignment_1_Backend.controllers.handlers;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;

import org.springframework.stereotype.Controller;
import project.Assignment_1_Backend.entities.SensorEvent;

@Controller
public class WebSocketController {

    @MessageMapping("/addEvent")
    @SendTo("/topic/events")
    public SensorEvent sendEvent(@Payload SensorEvent event){
        System.out.println("Sent!" + event);
        return event;
    }

}
