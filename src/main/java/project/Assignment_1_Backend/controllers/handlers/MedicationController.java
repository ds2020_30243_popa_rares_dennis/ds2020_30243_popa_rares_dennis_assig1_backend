package project.Assignment_1_Backend.controllers.handlers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import project.Assignment_1_Backend.dtos.MedicationDTO;
import project.Assignment_1_Backend.services.MedicationService;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/medication")
public class MedicationController {

    private final MedicationService medicationService;

    @Autowired
    public MedicationController(MedicationService medicationService){
        this.medicationService = medicationService;
    }

    @PostMapping(value = "/insert")
    public ResponseEntity<Long> insertMedication(@Valid @RequestBody MedicationDTO medicationDTO) {
        Long medicationId = medicationService.insert(medicationDTO);
        return new ResponseEntity<>(medicationId, HttpStatus.CREATED);
    }

    @PutMapping(value = "/updateMedication")
    public ResponseEntity<MedicationDTO> updateMedication(@Valid @RequestBody MedicationDTO medicationDTO){
        medicationDTO = medicationService.saveMedication(medicationDTO);
        return new ResponseEntity<>(medicationDTO,HttpStatus.OK);
    }

    @DeleteMapping(value = "/deleteMedication/{medicationId}")
    public boolean deleteMedicationById(@PathVariable(name = "medicationId")Long medicationId){

        return medicationService.deleteMedicationById(medicationId);
    }

    @GetMapping(value = "/getMedications")
    public ResponseEntity<List<MedicationDTO>> getMedications(){
        List<MedicationDTO> medicationDTOS = medicationService.getMedications();
        return new ResponseEntity<>(medicationDTOS,HttpStatus.OK);
    }

}
