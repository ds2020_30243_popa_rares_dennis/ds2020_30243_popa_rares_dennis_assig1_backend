package project.Assignment_1_Backend.controllers.handlers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import project.Assignment_1_Backend.dtos.MedicationPlanDTO;
import project.Assignment_1_Backend.dtos.PatientDTO;
import project.Assignment_1_Backend.dtos.UserDTO;
import project.Assignment_1_Backend.services.MedicationPlanService;
import project.Assignment_1_Backend.services.PatientService;
import project.Assignment_1_Backend.services.UserService;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/patient")
public class PatientController {

    private final UserService userService;
    private final PatientService patientService;
    private final MedicationPlanService medicationPlanService;

    @Autowired
    public PatientController(UserService userService,PatientService patientService,MedicationPlanService medicationPlanService){
        this.userService = userService;
        this.patientService = patientService;
        this.medicationPlanService = medicationPlanService;
    }

    @PostMapping(value = "/insert")
    public ResponseEntity<Long> insertPatient(@Valid @RequestBody PatientDTO patientDTO) {
        String encoded = new BCryptPasswordEncoder().encode(patientDTO.getUser().getPassword());
        patientDTO.getUser().setPassword(encoded);
        Long userId = userService.insertUser(patientDTO.getUser());
        patientDTO.getUser().setId(userId);
        Long patientId = patientService.insert(patientDTO);
        return new ResponseEntity<>(patientId, HttpStatus.CREATED);
    }

    @PutMapping(value = "/updatePatient")
    public ResponseEntity<PatientDTO> updatePatient(@Valid @RequestBody PatientDTO patientDTO){
        String encoded = new BCryptPasswordEncoder().encode(patientDTO.getUser().getPassword());
        patientDTO.getUser().setPassword(encoded);
        UserDTO userDTO = userService.saveUser(patientDTO.getUser());
        patientDTO.setUser(userDTO);
        patientDTO = patientService.savePatient(patientDTO);
        return new ResponseEntity<>(patientDTO,HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete/{patientId}")
    public boolean deletePatientById(@PathVariable(name = "patientId")Long patientId){
        PatientDTO patientDTO = patientService.findPatientById(patientId);
        UserDTO userDTO = userService.findUserById(patientDTO.getUser().getId());
        userService.deleteUserById(userDTO.getId());
        patientService.deletePatientById(patientDTO.getId());
        return true;
    }

    @PutMapping(value = "/addMedicationPlan/{patientId}/{medicationPlanId}")
    public ResponseEntity<PatientDTO> addMedicationPlan(@PathVariable(name = "patientId") Long patientId,
                                                        @PathVariable(name = "medicationPlanId") Long medicationPlanId){

        PatientDTO patientDTO = patientService.findPatientById(patientId);
        MedicationPlanDTO medicationPlanDTO = medicationPlanService.findMedicationPlanById(medicationPlanId);
        patientDTO.getMedicationPlans().add(medicationPlanDTO);
        patientDTO = patientService.savePatient(patientDTO);
        return new ResponseEntity<>(patientDTO, HttpStatus.OK);
    }

    @PutMapping(value = "/removeMedicationPlan/{patientId}/{medicationPlanId}")
    public ResponseEntity<PatientDTO> removeMedicationPlan(@PathVariable(name = "patientId") Long patientId,
                                                        @PathVariable(name = "medicationPlanId") Long medicationPlanId){

        PatientDTO patientDTO = patientService.findPatientById(patientId);
        MedicationPlanDTO medicationPlanDTO = medicationPlanService.findMedicationPlanById(medicationPlanId);
        patientDTO.getMedicationPlans().remove(medicationPlanDTO);
        patientDTO = patientService.savePatient(patientDTO);
        return new ResponseEntity<>(patientDTO, HttpStatus.OK);
    }

    @GetMapping(value = "/getPatients")
    public ResponseEntity<List<PatientDTO>> getPatients(){
        List<PatientDTO> patientDTOS = patientService.getAllPatients();
        return new ResponseEntity<>(patientDTOS,HttpStatus.OK);
    }

    @GetMapping(value = "/findByUser/{userId}")
    public ResponseEntity<PatientDTO> findByUser(@PathVariable(name = "userId") Long userId)
    {
        UserDTO userDTO = userService.findUserById(userId);
        PatientDTO patientDTO = patientService.findPatientByUser(userDTO);
        return new ResponseEntity<>(patientDTO, HttpStatus.OK);
    }


}
