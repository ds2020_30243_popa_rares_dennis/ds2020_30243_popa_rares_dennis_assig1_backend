package project.Assignment_1_Backend.controllers.handlers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import project.Assignment_1_Backend.dtos.MedicDTO;
import project.Assignment_1_Backend.dtos.UserDTO;
import project.Assignment_1_Backend.services.MedicService;
import project.Assignment_1_Backend.services.UserService;

import javax.validation.Valid;

@RestController
@CrossOrigin
@RequestMapping(value = "/medic")
public class MedicController {

    private final UserService userService;
    private final MedicService medicService;

    @Autowired
    public MedicController(UserService userService,MedicService medicService){
        this.userService = userService;
        this.medicService = medicService;
    }

    @PostMapping(value = "/insert")
    public ResponseEntity<Long> insertMedic(@Valid @RequestBody MedicDTO medicDTO) {
        String encoded = new BCryptPasswordEncoder().encode(medicDTO.getUser().getPassword());
        medicDTO.getUser().setPassword(encoded);
        Long userId = userService.insertUser(medicDTO.getUser());
        medicDTO.getUser().setId(userId);
        Long medicId = medicService.insert(medicDTO);
        return new ResponseEntity<>(medicId, HttpStatus.CREATED);
    }

    @PutMapping(value = "/updateMedic")
    public ResponseEntity<MedicDTO> updateMedic(@Valid @RequestBody MedicDTO medicDTO){
        String encoded = new BCryptPasswordEncoder().encode(medicDTO.getUser().getPassword());
        medicDTO.getUser().setPassword(encoded);
        UserDTO userDTO = userService.saveUser(medicDTO.getUser());
        medicDTO.setUser(userDTO);
        MedicDTO response = medicService.saveMedic(medicDTO);
        return new ResponseEntity<>(response,HttpStatus.OK);
    }

    @DeleteMapping(value = "/deleteMedic/{medicId}")
    public boolean deleteMedicById(@PathVariable(name = "medicId") Long medicId){
        MedicDTO medicDTO = medicService.findMedicById(medicId);
        if(medicDTO != null) {
            UserDTO userDTO = userService.findUserById(medicDTO.getUser().getId());
            userService.deleteUserById(userDTO.getId());
            medicService.deleteMedicById(medicDTO.getId());
            return true;
        }
        return false;
    }

}
