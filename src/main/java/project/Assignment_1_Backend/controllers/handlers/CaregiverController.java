package project.Assignment_1_Backend.controllers.handlers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import project.Assignment_1_Backend.dtos.CaregiverDTO;
import project.Assignment_1_Backend.dtos.PatientDTO;
import project.Assignment_1_Backend.dtos.UserDTO;
import project.Assignment_1_Backend.services.CaregiverService;
import project.Assignment_1_Backend.services.PatientService;
import project.Assignment_1_Backend.services.UserService;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/caregiver")
public class CaregiverController {

    private final UserService userService;
    private final CaregiverService caregiverService;
    private final PatientService patientService;

    @Autowired
    public CaregiverController(UserService userService,CaregiverService caregiverService,PatientService patientService){
        this.userService = userService;
        this.caregiverService = caregiverService;
        this.patientService = patientService;
    }

    @PostMapping(value = "/insert")
    public ResponseEntity<Long> insertCaregiver(@Valid @RequestBody CaregiverDTO caregiverDTO) {
        String encoded = new BCryptPasswordEncoder().encode(caregiverDTO.getUser().getPassword());
        caregiverDTO.getUser().setPassword(encoded);
        Long userId = userService.insertUser(caregiverDTO.getUser());
        caregiverDTO.getUser().setId(userId);
        Long caregiverId = caregiverService.insert(caregiverDTO);
        return new ResponseEntity<>(caregiverId, HttpStatus.CREATED);
    }

    @PutMapping(value = "/updateCaregiver")
    public ResponseEntity<CaregiverDTO> updateCaregiver(@Valid @RequestBody CaregiverDTO caregiverDTO){
        String encoded = new BCryptPasswordEncoder().encode(caregiverDTO.getUser().getPassword());
        caregiverDTO.getUser().setPassword(encoded);
        UserDTO userDTO = userService.saveUser(caregiverDTO.getUser());
        caregiverDTO.setUser(userDTO);
        caregiverDTO = caregiverService.saveCaregiver(caregiverDTO);
        return new ResponseEntity<>(caregiverDTO,HttpStatus.OK);
    }

    @GetMapping(value = "/getCaregivers")
    public ResponseEntity<List<CaregiverDTO>> getCaregivers(){
        List<CaregiverDTO> caregivers = caregiverService.findAllCaregivers();
        return new ResponseEntity<>(caregivers,HttpStatus.OK);
    }

    @PutMapping(value = "/addPatient/{caregiverId}/{patientId}")
    public ResponseEntity<CaregiverDTO> addPatient(@PathVariable(name = "caregiverId") Long caregiverId,
                                                   @PathVariable(name = "patientId") Long patientId){

        CaregiverDTO caregiverDTO = caregiverService.findCaregiverById(caregiverId);
        PatientDTO patientDTO = patientService.findPatientById(patientId);
        caregiverDTO.getPatients().add(patientDTO);
        caregiverDTO = caregiverService.saveCaregiver(caregiverDTO);
        return new ResponseEntity<>(caregiverDTO,HttpStatus.OK);
    }

    @PutMapping(value = "/removePatient/{caregiverId}/{patientId}")
    public ResponseEntity<CaregiverDTO> removePatient(@PathVariable(name = "caregiverId") Long caregiverId,
                                                      @PathVariable(name = "patientId") Long patientId){
        CaregiverDTO caregiverDTO = caregiverService.findCaregiverById(caregiverId);
        PatientDTO patientDTO = patientService.findPatientById(patientId);
        caregiverDTO.getPatients().remove(patientDTO);
        caregiverDTO = caregiverService.saveCaregiver(caregiverDTO);
        return new ResponseEntity<>(caregiverDTO,HttpStatus.OK);
    }

    @DeleteMapping(value = "/deleteCaregiver/{caregiverId}")
    public boolean deleteCaregiverById(@PathVariable(name = "caregiverId")Long caregiverId){
        CaregiverDTO caregiverDTO = caregiverService.findCaregiverById(caregiverId);
        UserDTO userDTO = userService.findUserById(caregiverDTO.getUser().getId());
        userService.deleteUserById(userDTO.getId());
        caregiverService.deleteCaregiverById(caregiverDTO.getId());
        return true;
    }

    @GetMapping(value = "/findByUser/{userId}")
    public ResponseEntity<CaregiverDTO> findByUser(@PathVariable(name = "userId") Long userId)
    {
        UserDTO userDTO = userService.findUserById(userId);
        CaregiverDTO caregiverDTO = caregiverService.findCaregiverByUser(userDTO);
        return new ResponseEntity<>(caregiverDTO, HttpStatus.OK);
    }

}
