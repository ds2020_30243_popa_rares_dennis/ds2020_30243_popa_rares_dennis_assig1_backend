package project.Assignment_1_Backend.controllers.handlers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import project.Assignment_1_Backend.dtos.MedicationDTO;
import project.Assignment_1_Backend.dtos.MedicationPlanDTO;
import project.Assignment_1_Backend.services.MedicationPlanService;
import project.Assignment_1_Backend.services.MedicationService;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/medicationPlan")
public class MedicationPlanController {

    private final MedicationPlanService medicationPlanService;
    private final MedicationService medicationService;

    @Autowired
    public MedicationPlanController(MedicationPlanService medicationPlanService,MedicationService medicationService){
        this.medicationPlanService = medicationPlanService;
        this.medicationService = medicationService;
    }

    @PostMapping(value = "/insert")
    public ResponseEntity<Long> insertMedication(@Valid @RequestBody MedicationPlanDTO medicationPlanDTO) {
        Long planId = medicationPlanService.insert(medicationPlanDTO);
        return new ResponseEntity<>(planId,HttpStatus.CREATED);
    }

    @PutMapping(value = "/addMedication/{medicationplanId}/{medicationId}")
    public ResponseEntity<MedicationPlanDTO> addMedication(@PathVariable(name = "medicationplanId") Long medicationplanId,
                                                           @PathVariable(name = "medicationId") Long medicationId){
        MedicationDTO medicationDTO = medicationService.findById(medicationId);
        MedicationPlanDTO medicationPlanDTO = medicationPlanService.findMedicationPlanById(medicationplanId);
        medicationPlanDTO.getMedications().add(medicationDTO);
        medicationPlanDTO = medicationPlanService.saveMedicationPlan(medicationPlanDTO);

        return new ResponseEntity<>(medicationPlanDTO,HttpStatus.OK);
    }

    @PutMapping(value = "/removeMedication/{medicationplanId}/{medicationId}")
    public ResponseEntity<MedicationPlanDTO> removeMedication(@PathVariable(name = "medicationplanId") Long medicationplanId,
                                                           @PathVariable(name = "medicationId") Long medicationId){
        MedicationDTO medicationDTO = medicationService.findById(medicationId);
        MedicationPlanDTO medicationPlanDTO = medicationPlanService.findMedicationPlanById(medicationplanId);
        medicationPlanDTO.getMedications().remove(medicationDTO);
        medicationPlanDTO = medicationPlanService.saveMedicationPlan(medicationPlanDTO);
        return new ResponseEntity<>(medicationPlanDTO,HttpStatus.OK);
    }

    @GetMapping(value = "/getAllMedicationPlans")
    public ResponseEntity<List<MedicationPlanDTO>> getMedicationPlans(){
        List<MedicationPlanDTO> medicationPlanDTOS = medicationPlanService.findAllMedicationPlans();
        return new ResponseEntity<>(medicationPlanDTOS,HttpStatus.OK);
    }

    @PutMapping(value = "/updateMedicationPlan")
    public ResponseEntity<MedicationPlanDTO> updateMedicationPlan(@Valid @RequestBody MedicationPlanDTO medicationPlanDTO){
        medicationPlanDTO = medicationPlanService.saveMedicationPlan(medicationPlanDTO);
        return new ResponseEntity<>(medicationPlanDTO,HttpStatus.OK);
    }

    @DeleteMapping(value = "/deleteMedicationPlan/{medicationplanId}")
    public boolean deleteMedicationPlanById(Long medicationPlanId){
        return medicationPlanService.deleteMedicationPlanById(medicationPlanId);
    }

}
