package project.Assignment_1_Backend.hessian;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import project.Assignment_1_Backend.dtos.MedicationPlanDTO;
import project.Assignment_1_Backend.dtos.MedicationTakenDTO;
import project.Assignment_1_Backend.dtos.PatientDTO;
import project.Assignment_1_Backend.dtos.UserDTO;
import project.Assignment_1_Backend.services.MedicationPlanService;
import project.Assignment_1_Backend.services.MedicationTakenService;
import project.Assignment_1_Backend.services.PatientService;
import project.Assignment_1_Backend.services.UserService;

import java.util.ArrayList;
import java.util.List;

public class RpcImplementation implements RpcInterface {

    private final MedicationTakenService medicationTakenService;
    private final MedicationPlanService medicationPlanService;
    private final UserService userService;
    private final PatientService patientService;

    private static final Gson GSON = new Gson();

    public RpcImplementation(MedicationTakenService medicationTakenService, MedicationPlanService medicationPlanService,UserService userService,PatientService patientService) {
        this.medicationTakenService = medicationTakenService;
        this.medicationPlanService = medicationPlanService;
        this.userService = userService;
        this.patientService = patientService;
    }

    @Override
    public Long insertMedicationTaken(String medicationTakenDTO) {
        return medicationTakenService.insert(GSON.fromJson(medicationTakenDTO,MedicationTakenDTO.class));
    }

    @Override
    public List<String> getMedicationPlans() {
        List<MedicationPlanDTO> medicationPlanDTOS = medicationPlanService.findAllMedicationPlans();
        List<String> gsons = new ArrayList<>();
        for(MedicationPlanDTO plan : medicationPlanDTOS){
            gsons.add(GSON.toJson(plan,MedicationPlanDTO.class));
        }
        return gsons;
    }

    @Override
    public String findUserByEmail(String email) {
        return GSON.toJson(userService.findUserByEmail(email),UserDTO.class);
    }

    @Override
    public String checkLogin(String email, String password) {
        UserDTO userDTO = userService.findUserByEmail(email);
        if(userDTO != null) {
            boolean ret = new BCryptPasswordEncoder().matches(password, userDTO.getPassword());
            if(ret)
                return GSON.toJson(userDTO,UserDTO.class);
            else return "";
        }else return "";
    }

    @Override
    public String getPatientByUserId(Long userId) {
        UserDTO userDTO = userService.findUserById(userId);
        PatientDTO patientDTO = patientService.findPatientByUser(userDTO);
        return GSON.toJson(patientDTO,PatientDTO.class);
    }

    @Override
    public String findMedicationPlanById(Long medicationPlanId) {
        MedicationPlanDTO medicationPlanDTO = medicationPlanService.findMedicationPlanById(medicationPlanId);
        return GSON.toJson(medicationPlanDTO,MedicationPlanDTO.class);
    }

    @Override
    public String findPatientByUser(String userDTO){
        UserDTO user = GSON.fromJson(userDTO,UserDTO.class);
        PatientDTO patient = patientService.findPatientByUser(user);
        return GSON.toJson(patient,PatientDTO.class);
    }

}
