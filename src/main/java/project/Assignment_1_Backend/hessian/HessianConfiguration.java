package project.Assignment_1_Backend.hessian;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.caucho.HessianServiceExporter;
import org.springframework.remoting.support.RemoteExporter;
import project.Assignment_1_Backend.services.MedicationPlanService;
import project.Assignment_1_Backend.services.MedicationTakenService;
import project.Assignment_1_Backend.services.PatientService;
import project.Assignment_1_Backend.services.UserService;

@Configuration
public class HessianConfiguration {

    @Autowired
    private MedicationTakenService medicationTakenService;
    @Autowired
    private MedicationPlanService medicationPlanService;
    @Autowired
    private UserService userService;
    @Autowired
    private PatientService patientService;



    @Bean(name = "/hessianRpc")
    RemoteExporter sayHelloServiceHessian() {
        HessianServiceExporter exporter = new HessianServiceExporter();
        exporter.setService(new RpcImplementation(medicationTakenService,medicationPlanService,userService,patientService));
        exporter.setServiceInterface(RpcInterface.class);
        return exporter;
    }

}
