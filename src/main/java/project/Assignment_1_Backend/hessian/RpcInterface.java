package project.Assignment_1_Backend.hessian;

import project.Assignment_1_Backend.dtos.UserDTO;

import java.io.Serializable;
import java.util.List;


public interface RpcInterface extends Serializable {
    public Long insertMedicationTaken(String medicationTakenDTO);
    public List<String> getMedicationPlans();
    public String findMedicationPlanById(Long medicationPlanId);
    public String findPatientByUser(String userDTO);
    public String findUserByEmail(String email);
    public String checkLogin(String email,String password);
    public String getPatientByUserId(Long userId);
}
