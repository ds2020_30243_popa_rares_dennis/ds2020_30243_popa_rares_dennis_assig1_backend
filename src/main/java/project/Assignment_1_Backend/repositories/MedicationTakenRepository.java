package project.Assignment_1_Backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import project.Assignment_1_Backend.entities.MedicationTaken;

public interface MedicationTakenRepository extends JpaRepository<MedicationTaken, Long> {

}
