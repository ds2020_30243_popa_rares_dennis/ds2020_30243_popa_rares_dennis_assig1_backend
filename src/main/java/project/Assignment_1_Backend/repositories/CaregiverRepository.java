package project.Assignment_1_Backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import project.Assignment_1_Backend.entities.Caregiver;
import project.Assignment_1_Backend.entities.User;

public interface CaregiverRepository extends JpaRepository<Caregiver, Long> {
    public Caregiver findByUser (User user);
}
