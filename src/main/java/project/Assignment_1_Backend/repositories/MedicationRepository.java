package project.Assignment_1_Backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import project.Assignment_1_Backend.entities.Medication;

public interface MedicationRepository extends JpaRepository<Medication, Long> {
}
