package project.Assignment_1_Backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import project.Assignment_1_Backend.entities.User;


public interface UserRepository extends JpaRepository<User, Long> {

    public User findByEmail(String email);
}
