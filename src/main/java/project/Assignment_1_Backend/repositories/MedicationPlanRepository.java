package project.Assignment_1_Backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import project.Assignment_1_Backend.entities.MedicationPlan;

public interface MedicationPlanRepository extends JpaRepository<MedicationPlan, Long> {
}
