package project.Assignment_1_Backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import project.Assignment_1_Backend.entities.Patient;
import project.Assignment_1_Backend.entities.User;


public interface PatientRepository extends JpaRepository<Patient, Long> {
    public Patient findByUser(User user);
}
