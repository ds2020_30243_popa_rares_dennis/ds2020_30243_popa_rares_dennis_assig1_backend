package project.Assignment_1_Backend.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.Assignment_1_Backend.dtos.MedicationDTO;
import project.Assignment_1_Backend.dtos.MedicationTakenDTO;
import project.Assignment_1_Backend.entities.Medication;
import project.Assignment_1_Backend.entities.MedicationTaken;
import project.Assignment_1_Backend.repositories.MedicationRepository;
import project.Assignment_1_Backend.repositories.MedicationTakenRepository;
import project.Assignment_1_Backend.utils.EntityConvertor;

import java.io.Serializable;

@Service
public class MedicationTakenService implements Serializable {

    private final MedicationTakenRepository medicationTakenRepository;

    @Autowired
    public MedicationTakenService(MedicationTakenRepository medicationTakenRepository) {
        this.medicationTakenRepository = medicationTakenRepository;
    }

    public Long insert(MedicationTakenDTO medicationTakenDTO){
        MedicationTaken medicationTaken = EntityConvertor.convertToMedicationTakenEntity(medicationTakenDTO);
        medicationTakenRepository.save(medicationTaken);
        return medicationTaken.getId();
    }

}
