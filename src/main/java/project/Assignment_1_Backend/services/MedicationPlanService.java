package project.Assignment_1_Backend.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.Assignment_1_Backend.dtos.MedicationPlanDTO;
import project.Assignment_1_Backend.entities.MedicationPlan;
import project.Assignment_1_Backend.repositories.MedicationPlanRepository;
import project.Assignment_1_Backend.utils.EntityConvertor;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MedicationPlanService implements Serializable {

    private final MedicationPlanRepository medicationPlanRepository;

    @Autowired
    public MedicationPlanService(MedicationPlanRepository medicationPlanRepository) {
        this.medicationPlanRepository = medicationPlanRepository;
    }

    public Long insert(MedicationPlanDTO medicationPlanDTO){
        MedicationPlan medicationPlan = EntityConvertor.convertToMedicationPlanEntity(medicationPlanDTO);
        medicationPlanRepository.save(medicationPlan);
        return medicationPlan.getId();
    }

    public List<MedicationPlanDTO> findAllMedicationPlans(){
        List<MedicationPlan> medicationPlanList = medicationPlanRepository.findAll();
        return medicationPlanList.stream()
                .map(EntityConvertor::convertToMedicationPlanDTO)
                .collect(Collectors.toList());
    }

    public MedicationPlanDTO findMedicationPlanById(Long medicationPlanId){
        Optional<MedicationPlan> medicationPlan = medicationPlanRepository.findById(medicationPlanId);
        if(medicationPlan.isPresent())
            return EntityConvertor.convertToMedicationPlanDTO(medicationPlan.get());
        else
            return null;
    }

    public MedicationPlanDTO saveMedicationPlan(MedicationPlanDTO medicationPlanDTO){
        MedicationPlan medicationPlan = EntityConvertor.copyToMedicationPlanEntity(medicationPlanDTO);
        medicationPlanRepository.save(medicationPlan);
        return EntityConvertor.convertToMedicationPlanDTO(medicationPlan);
    }

    public boolean deleteMedicationPlanById(Long id){
        medicationPlanRepository.deleteById(id);
        return true;
    }
}
