package project.Assignment_1_Backend.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.Assignment_1_Backend.dtos.MedicationDTO;
import project.Assignment_1_Backend.entities.Medication;
import project.Assignment_1_Backend.repositories.MedicationRepository;
import project.Assignment_1_Backend.utils.EntityConvertor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
public class MedicationService implements Serializable {

    private final MedicationRepository medicationRepository;

    @Autowired
    public MedicationService(MedicationRepository medicationRepository) {
        this.medicationRepository = medicationRepository;
    }

    public Long insert(MedicationDTO medicationDTO){
        Medication medication = EntityConvertor.convertToMedicationEntity(medicationDTO);
        medicationRepository.save(medication);
        return medication.getId();
    }
    
    public MedicationDTO findById(Long medicationId){
        Optional<Medication> medication = medicationRepository.findById(medicationId);
        if(medication.isPresent())
            return EntityConvertor.convertToMedicationDTO(medication.get());
        else
            return null;
    }

    public MedicationDTO saveMedication(MedicationDTO medicationDTO){
        Medication medication = EntityConvertor.copyToMedicationEntity(medicationDTO);
        medication = medicationRepository.save(medication);
        return EntityConvertor.convertToMedicationDTO(medication);
    }

    public boolean deleteMedicationById(Long id){
        try {
            medicationRepository.deleteById(id);
            return true;
        }catch(Exception e){
            return false;
        }
    }

    public List<MedicationDTO> getMedications(){
        List<Medication> medications = medicationRepository.findAll();
        if(medications != null) {
            return medications.stream()
                    .map(EntityConvertor::convertToMedicationDTO)
                    .collect(Collectors.toList());
        }else return new ArrayList<MedicationDTO>();
    }

}
