package project.Assignment_1_Backend.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.Assignment_1_Backend.dtos.MedicDTO;
import project.Assignment_1_Backend.entities.Medic;
import project.Assignment_1_Backend.repositories.MedicRepository;
import project.Assignment_1_Backend.utils.EntityConvertor;

import java.util.Optional;


@Service
public class MedicService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MedicService.class);
    private final MedicRepository medicRepository;

    @Autowired
    public MedicService(MedicRepository medicRepository) {
        this.medicRepository = medicRepository;
    }

    public Long insert(MedicDTO medicDTO){
        if(medicDTO != null) {
            Medic medic = EntityConvertor.convertToMedicEntity(medicDTO);
            medicRepository.save(medic);
            LOGGER.debug("Medic with id {} was inserted in db", medic.getId());
            return medic.getId();
        }else return null;
    }

    public MedicDTO saveMedic(MedicDTO medicDTO){
        if(medicDTO != null) {
            Medic medic = medicRepository.save(EntityConvertor.copyToMedicEntity(medicDTO));
            return EntityConvertor.convertToMedicDTO(medic);
        }else return null;
    }

    public boolean deleteMedicById(Long id){
        try {
            medicRepository.deleteById(id);
            return true;
        }catch(Exception e){
            return false;
        }
    }

    public MedicDTO findMedicById(Long id){
        Optional<Medic> medic = medicRepository.findById(id);
        if(medic.isPresent())
            return EntityConvertor.convertToMedicDTO(medic.get());
        else
            return null;
    }

}
