package project.Assignment_1_Backend.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.Assignment_1_Backend.dtos.UserDTO;
import project.Assignment_1_Backend.entities.User;
import project.Assignment_1_Backend.repositories.UserRepository;
import project.Assignment_1_Backend.utils.EntityConvertor;

import java.util.Optional;



@Service
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Long insertUser(UserDTO userDTO){
        if(userDTO != null) {
            User user = EntityConvertor.convertToUserEntity(userDTO);

            userRepository.save(user);
            return user.getId();
        }else
            return null;
    }

    public UserDTO saveUser(UserDTO userDTO){
        if(userDTO != null) {
            User user = EntityConvertor.copyToUserEntity(userDTO);
            userRepository.save(user);
            return userDTO;
        }else
            return null;
    }

    public UserDTO findUserByEmail(String email){
        User user = null;
        try {
             user = userRepository.findByEmail(email);
             if(user != null)
                return EntityConvertor.convertToUserDTO(user);
             else
                 return null;
        }catch (Exception ex){
            return null;
        }
    }

    public boolean deleteUserById(Long id){
        try {
            userRepository.deleteById(id);
            return true;
        }
        catch(Exception e){
            return false;
        }
    }

    public UserDTO findUserById(Long id){
        Optional<User> user = userRepository.findById(id);
        if(user.isPresent())
            return EntityConvertor.convertToUserDTO(user.get());
        else
            return null;
    }



}
