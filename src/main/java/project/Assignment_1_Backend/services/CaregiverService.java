package project.Assignment_1_Backend.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.Assignment_1_Backend.dtos.CaregiverDTO;
import project.Assignment_1_Backend.dtos.UserDTO;
import project.Assignment_1_Backend.entities.Caregiver;
import project.Assignment_1_Backend.repositories.CaregiverRepository;
import project.Assignment_1_Backend.utils.EntityConvertor;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CaregiverService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CaregiverService.class);
    private final CaregiverRepository caregiverRepository;

    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository) {
        this.caregiverRepository = caregiverRepository;
    }

    public Long insert(CaregiverDTO caregiverDTO){
        caregiverDTO.setPatients(new ArrayList<>());
        Caregiver caregiver = EntityConvertor.convertToCaregiverEntity(caregiverDTO);
        caregiverRepository.save(caregiver);
        LOGGER.debug("Caregiver with id {} was inserted in db", caregiver.getId());
        return caregiver.getId();
    }

    public List<CaregiverDTO> findAllCaregivers(){
        return caregiverRepository.findAll().stream()
                .map(EntityConvertor::convertToCaregiverDTO)
                .collect(Collectors.toList());
    }

    public CaregiverDTO saveCaregiver(CaregiverDTO caregiverDTO){
        Caregiver caregiver =  caregiverRepository.save(EntityConvertor.copyToCaregiverEntity(caregiverDTO));
        return EntityConvertor.convertToCaregiverDTO(caregiver);
    }

    public CaregiverDTO findCaregiverById(Long id){
        Optional<Caregiver> caregiver = caregiverRepository.findById(id);
        if(caregiver.isPresent())
            return EntityConvertor.convertToCaregiverDTO(caregiver.get());
        else
            return null;
    }

    public boolean deleteCaregiverById(Long id){
        caregiverRepository.deleteById(id);
        return true;
    }

    public CaregiverDTO findCaregiverByUser(UserDTO userDTO){
        Caregiver caregiver = caregiverRepository.findByUser(EntityConvertor.copyToUserEntity(userDTO));
        return EntityConvertor.convertToCaregiverDTO(caregiver);
    }

}
