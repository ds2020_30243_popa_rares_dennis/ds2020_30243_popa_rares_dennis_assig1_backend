package project.Assignment_1_Backend.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.Assignment_1_Backend.dtos.PatientDTO;
import project.Assignment_1_Backend.dtos.UserDTO;
import project.Assignment_1_Backend.entities.Patient;
import project.Assignment_1_Backend.repositories.PatientRepository;
import project.Assignment_1_Backend.utils.EntityConvertor;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
public class PatientService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PatientService.class);
    private final PatientRepository patientRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    public Long insert(PatientDTO patientDTO){
        if(patientDTO != null) {
            Patient patient = EntityConvertor.convertToPatientEntity(patientDTO);
            patientRepository.save(patient);
            LOGGER.debug("Patient with id {} was inserted in db", patient.getId());
            return patient.getId();
        }else return null;
    }



    public PatientDTO savePatient(PatientDTO patientDTO){
        if(patientDTO != null) {
            Patient patient = patientRepository.save(EntityConvertor.copyToPatientEntity(patientDTO));
            return EntityConvertor.convertToPatientDTO(patient);
        }else return null;
    }

    public PatientDTO findPatientById(Long id){
        Optional<Patient> patient = patientRepository.findById(id);
        if(patient.isPresent())
            return EntityConvertor.convertToPatientDTO(patient.get());
        else
            return null;
    }

    public boolean deletePatientById(Long patientId){
        try {
            patientRepository.deleteById(patientId);
            return true;
        }catch(Exception e){
            return false;
        }
    }

    public List<PatientDTO> getAllPatients(){

        List<Patient> patientList = patientRepository.findAll();
        if(patientList != null)
            return patientList.stream()
                .map(EntityConvertor::convertToPatientDTO)
                .collect(Collectors.toList());
        else
            return new ArrayList<PatientDTO>();
    }

    public PatientDTO findPatientByUser(UserDTO userDTO){
        Patient patient = patientRepository.findByUser(EntityConvertor.copyToUserEntity(userDTO));
        return EntityConvertor.convertToPatientDTO(patient);
    }

}
