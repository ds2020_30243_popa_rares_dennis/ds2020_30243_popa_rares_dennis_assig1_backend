package project.Assignment_1_Backend.rabbitmq;


import com.google.gson.Gson;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import project.Assignment_1_Backend.controllers.handlers.WebSocketController;
import project.Assignment_1_Backend.entities.SensorEvent;

@Service
public class RabbitMQListner implements MessageListener {



    private SimpMessagingTemplate messageSender;

    @Autowired
    public RabbitMQListner(SimpMessagingTemplate messageSender){
        this.messageSender = messageSender;
    }

    public void onMessage(Message message) {
        Gson g = new Gson();
        SensorEvent event = g.fromJson(new String(message.getBody()),SensorEvent.class);
        
        float duration = ((event.getFinishingTime()-event.getStartingTime())/1000.0f)/60.0f;
        try {
            if (event.getName().equals("sleeping")) {
                if (duration / 60.0f > 7.0f) {
                    System.out.println(event);
                    messageSender.convertAndSend("/topic/events", event);
                }
            }
            if (event.getName().equals("toileting") || event.getName().equals("showering")) {
                if (duration > 30) {
                    System.out.println(event);
                    messageSender.convertAndSend("/topic/events", event);
                }
            }
            if (event.getName().equals("leaving")) {
                if (duration / 60.0f > 5.0f) {
                    System.out.println(event);
                    messageSender.convertAndSend("/topic/events", event);
                }
            }
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }

    }

}
