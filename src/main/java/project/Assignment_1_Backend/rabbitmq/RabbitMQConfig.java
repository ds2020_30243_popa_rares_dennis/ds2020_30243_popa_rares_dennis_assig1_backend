package project.Assignment_1_Backend.rabbitmq;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.MessageListenerContainer;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.SimpMessagingTemplate;



@Configuration
public class RabbitMQConfig {

    private  RabbitMQListner listner;
    private  SimpMessagingTemplate sender;

    @Autowired
    public RabbitMQConfig(SimpMessagingTemplate sender){
        this.listner = new RabbitMQListner(sender);
        this.sender = sender;
    }


    private static final String queueName = "sensor.readings.queue";

    private static final String username = "hpcgspve";

    private static final String host = "roedeer.rmq.cloudamqp.com";

    private static final String password = "OfuEW0gNpXucJwjdmLmJa6qqgoAgvImU";

    private static final String virtualHost = "hpcgspve";



    @Bean
    Queue queue() {
        return new Queue(queueName, false);
    }


	@Bean
	ConnectionFactory connectionFactory() {
		CachingConnectionFactory cachingConnectionFactory = new CachingConnectionFactory();
		cachingConnectionFactory.setHost(host);
		cachingConnectionFactory.setUsername(username);
		cachingConnectionFactory.setPassword(password);
		cachingConnectionFactory.setVirtualHost(virtualHost);
		return cachingConnectionFactory;
	}

	@Bean
	MessageListenerContainer messageListenerContainer() {
		SimpleMessageListenerContainer simpleMessageListenerContainer = new SimpleMessageListenerContainer();
		simpleMessageListenerContainer.setConnectionFactory(connectionFactory());
		simpleMessageListenerContainer.addQueues(queue());
		simpleMessageListenerContainer.setMessageListener(this.listner);
		return simpleMessageListenerContainer;

	}

}