package project.Assignment_1_Backend.utils;

import java.io.Serializable;

public enum UserType implements Serializable {
    MEDIC,CAREGIVER,PATIENT
}
