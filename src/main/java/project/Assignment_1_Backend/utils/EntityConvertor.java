package project.Assignment_1_Backend.utils;

import project.Assignment_1_Backend.dtos.*;
import project.Assignment_1_Backend.entities.*;

import java.util.stream.Collectors;

public class EntityConvertor {

    private EntityConvertor (){}

    public static User convertToUserEntity (UserDTO userDTO){
        User user = new User();
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setAdress(userDTO.getAdress());
        user.setBirthdate(userDTO.getBirthdate());
        user.setEmail(userDTO.getEmail());
        user.setGender(userDTO.getGender());
        user.setPassword(userDTO.getPassword());
        user.setRole(userDTO.getRole());
        return user;
    }

    public static User copyToUserEntity(UserDTO userDTO){
        User user = new User();

        user.setId(userDTO.getId());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setAdress(userDTO.getAdress());
        user.setBirthdate(userDTO.getBirthdate());
        user.setEmail(userDTO.getEmail());
        user.setGender(userDTO.getGender());
        user.setPassword(userDTO.getPassword());
        user.setRole(userDTO.getRole());

        return user;
    }

    public static UserDTO convertToUserDTO(User user){

        UserDTO userDTO = new UserDTO();
        userDTO.setFirstName(user.getFirstName());
        userDTO.setLastName(user.getLastName());
        userDTO.setAdress(user.getAdress());
        userDTO.setBirthdate(user.getBirthdate());
        userDTO.setEmail(user.getEmail());
        userDTO.setGender(user.getGender());
        userDTO.setPassword(user.getPassword());
        userDTO.setRole(user.getRole());
        userDTO.setId(user.getId());

        return userDTO;

    }

    public static Patient convertToPatientEntity(PatientDTO patientDTO){
        Patient patient = new Patient();
        patient.setMedicalRecord(patientDTO.getMedicalRecord());
        patient.setUser(convertToUserEntity(patientDTO.getUser()));
        patient.getUser().setId(patientDTO.getUser().getId());

        patient.setMedicationPlans(patientDTO.getMedicationPlans().stream()
                .map(EntityConvertor::convertToMedicationPlanEntity)
                .collect(Collectors.toList()));
        return patient;
    }

    public static Patient copyToPatientEntity(PatientDTO patientDTO){

        Patient patient = new Patient();
        patient.setId(patientDTO.getId());
        patient.setMedicalRecord(patientDTO.getMedicalRecord());
        patient.setUser(copyToUserEntity(patientDTO.getUser()));
        patient.setMedicationPlans(patientDTO.getMedicationPlans().stream()
                .map(EntityConvertor::copyToMedicationPlanEntity)
                .collect(Collectors.toList()));
        return patient;

    }

    public static PatientDTO convertToPatientDTO(Patient patient){
        PatientDTO patientDTO = new PatientDTO();

        patientDTO.setId(patient.getId());
        patientDTO.setMedicalRecord(patient.getMedicalRecord());
        patientDTO.setUser(EntityConvertor.convertToUserDTO(patient.getUser()));
        patientDTO.setMedicationPlans(patient.getMedicationPlans().stream()
                .map(EntityConvertor::convertToMedicationPlanDTO)
                .collect(Collectors.toList()));

        return patientDTO;
    }

    public static Caregiver convertToCaregiverEntity(CaregiverDTO caregiverDTO){
        Caregiver caregiver = new Caregiver();
        caregiver.setPatients(caregiverDTO.getPatients().stream()
                .map(EntityConvertor::convertToPatientEntity)
                .collect(Collectors.toList()));
        caregiver.setUser(convertToUserEntity(caregiverDTO.getUser()));
        caregiver.getUser().setId(caregiverDTO.getUser().getId());
        return caregiver;
    }

    public static Caregiver copyToCaregiverEntity(CaregiverDTO caregiverDTO){

        Caregiver caregiver = new Caregiver();
        caregiver.setId(caregiverDTO.getId());
        caregiver.setPatients(caregiverDTO.getPatients().stream()
                .map(EntityConvertor::copyToPatientEntity)
                .collect(Collectors.toList()));
        caregiver.setUser(copyToUserEntity(caregiverDTO.getUser()));
        return caregiver;

    }

    public static CaregiverDTO convertToCaregiverDTO (Caregiver caregiver){
        CaregiverDTO caregiverDTO = new CaregiverDTO();

        caregiverDTO.setId(caregiver.getId());
        caregiverDTO.setUser(EntityConvertor.convertToUserDTO(caregiver.getUser()));
        caregiverDTO.setPatients(caregiver.getPatients().stream()
                .map(EntityConvertor::convertToPatientDTO)
                .collect(Collectors.toList()));

        return caregiverDTO;
    }

    public static Medic convertToMedicEntity(MedicDTO medicDTO){
        Medic medic = new Medic();
        medic.setUser(convertToUserEntity(medicDTO.getUser()));
        medic.getUser().setId(medicDTO.getUser().getId());
        return medic;
    }

    public static Medic copyToMedicEntity (MedicDTO medicDTO){
        Medic medic = new Medic();
        medic.setId(medicDTO.getId());
        medic.setUser(copyToUserEntity(medicDTO.getUser()));
        return medic;
    }

    public static MedicDTO convertToMedicDTO (Medic medic){
        MedicDTO medicDTO = new MedicDTO();

        medicDTO.setId(medic.getId());
        medicDTO.setUser(convertToUserDTO(medic.getUser()));

        return medicDTO;
    }

    public static Medication convertToMedicationEntity (MedicationDTO medicationDTO){
        Medication medication = new Medication();

        medication.setDosage(medicationDTO.getDosage());
        medication.setSideEffects(medicationDTO.getSideEffects());
        medication.setName(medicationDTO.getName());

        return medication;
    }

    public static Medication copyToMedicationEntity(MedicationDTO medicationDTO){
        Medication medication = new Medication();

        medication.setId(medicationDTO.getId());
        medication.setSideEffects(medicationDTO.getSideEffects());
        medication.setDosage(medicationDTO.getDosage());
        medication.setName(medicationDTO.getName());

        return medication;
    }

    public static MedicationDTO convertToMedicationDTO (Medication medication){
        MedicationDTO medicationDTO = new MedicationDTO();

        medicationDTO.setId(medication.getId());
        medicationDTO.setDosage(medication.getDosage());
        medicationDTO.setSideEffects(medication.getSideEffects());
        medicationDTO.setName(medication.getName());

        return medicationDTO;
    }

    public static MedicationPlan convertToMedicationPlanEntity(MedicationPlanDTO medicationPlanDTO){
        MedicationPlan medicationPlan = new MedicationPlan();

        medicationPlan.setEndDate(medicationPlanDTO.getEndDate());
        medicationPlan.setEndTime(medicationPlanDTO.getEndTime());
        medicationPlan.setMedications(medicationPlanDTO.getMedications().stream()
                .map(EntityConvertor::copyToMedicationEntity)
                .collect(Collectors.toList()));
        medicationPlan.setStartDate(medicationPlanDTO.getStartDate());
        medicationPlan.setStartTime(medicationPlanDTO.getStartTime());

        return medicationPlan;
    }

    public static MedicationPlan copyToMedicationPlanEntity(MedicationPlanDTO medicationPlanDTO){
        MedicationPlan medicationPlan = new MedicationPlan();

        medicationPlan.setId(medicationPlanDTO.getId());
        medicationPlan.setEndDate(medicationPlanDTO.getEndDate());
        medicationPlan.setEndTime(medicationPlanDTO.getEndTime());
        medicationPlan.setMedications(medicationPlanDTO.getMedications().stream()
                .map(EntityConvertor::copyToMedicationEntity)
                .collect(Collectors.toList()));
        medicationPlan.setStartDate(medicationPlanDTO.getStartDate());
        medicationPlan.setStartTime(medicationPlanDTO.getStartTime());

        return medicationPlan;
    }

    public static MedicationPlanDTO convertToMedicationPlanDTO(MedicationPlan medicationPlan){
        MedicationPlanDTO medicationPlanDTO = new MedicationPlanDTO();

        medicationPlanDTO.setId(medicationPlan.getId());
        medicationPlanDTO.setEndDate(medicationPlan.getEndDate());
        medicationPlanDTO.setEndTime(medicationPlan.getEndTime());
        medicationPlanDTO.setMedications(medicationPlan.getMedications().stream()
                .map(EntityConvertor::convertToMedicationDTO)
                .collect(Collectors.toList()));
        medicationPlanDTO.setStartDate(medicationPlan.getStartDate());
        medicationPlanDTO.setStartTime(medicationPlan.getStartTime());

        return medicationPlanDTO;
    }

    public static MedicationTaken convertToMedicationTakenEntity (MedicationTakenDTO medicationTakenDTO){
        MedicationTaken medicationTaken = new MedicationTaken();
        medicationTaken.setMedicationPlan(copyToMedicationPlanEntity(medicationTakenDTO.getMedicationPlanDTO()));
        medicationTaken.setPatientId(medicationTakenDTO.getPatientId());
        medicationTaken.setTaken(medicationTakenDTO.isTaken());
        return medicationTaken;
    }

    public static  MedicationTaken copyToMedicationTakenEntity (MedicationTakenDTO medicationTakenDTO){
        MedicationTaken medicationTaken = new MedicationTaken();
        medicationTaken.setId(medicationTakenDTO.getId());
        medicationTaken.setMedicationPlan(copyToMedicationPlanEntity(medicationTakenDTO.getMedicationPlanDTO()));
        medicationTaken.setPatientId(medicationTakenDTO.getPatientId());
        medicationTaken.setTaken(medicationTakenDTO.isTaken());
        return medicationTaken;
    }


    public static  MedicationTakenDTO convertToMedicationTakenDTO (MedicationTaken medicationTaken){
        MedicationTakenDTO medicationTakenDTO = new MedicationTakenDTO();
        medicationTakenDTO.setId(medicationTaken.getId());
        medicationTakenDTO.setMedicationPlanDTO(convertToMedicationPlanDTO(medicationTaken.getMedicationPlan()));
        medicationTakenDTO.setPatientId(medicationTaken.getPatientId());
        medicationTakenDTO.setTaken(medicationTaken.isTaken());
        return medicationTakenDTO;
    }
}
