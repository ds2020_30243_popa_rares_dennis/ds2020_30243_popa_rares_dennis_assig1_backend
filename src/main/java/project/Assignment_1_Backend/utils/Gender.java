package project.Assignment_1_Backend.utils;

import java.io.Serializable;

public enum Gender implements Serializable {
    MALE, FEMALE
}
