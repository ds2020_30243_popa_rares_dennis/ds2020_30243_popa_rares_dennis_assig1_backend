package project.Assignment_1_Backend.entities;

import java.io.Serializable;

public class SensorEvent implements Serializable {

    private Long startingTime;

    private Long finishingTime;

    private String name;

    private Long id = (long) -1;

    public Long getStartingTime() {
        return startingTime;
    }

    public void setStartingTime(Long startingTime) {
        this.startingTime = startingTime;
    }

    public Long getFinishingTime() {
        return finishingTime;
    }

    public void setFinishingTime(Long finishingTime) {
        this.finishingTime = finishingTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "SensorEvent{" +
                "startingTime=" + startingTime +
                ", finishingTime=" + finishingTime +
                ", name='" + name + '\'' +
                ", id=" + id +
                '}';
    }
}
