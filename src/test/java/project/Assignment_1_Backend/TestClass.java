package project.Assignment_1_Backend;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import project.Assignment_1_Backend.dtos.CaregiverDTO;
import project.Assignment_1_Backend.dtos.PatientDTO;
import project.Assignment_1_Backend.dtos.UserDTO;
import project.Assignment_1_Backend.entities.Caregiver;
import project.Assignment_1_Backend.entities.Patient;
import project.Assignment_1_Backend.entities.User;
import project.Assignment_1_Backend.utils.EntityConvertor;

import static org.junit.Assert.assertTrue;

@SpringBootTest
public class TestClass {

    @Test
    public void testEntityConvertor1(){
        Caregiver caregiver = new Caregiver();
        caregiver.setUser(new User());
        assertTrue(EntityConvertor.convertToCaregiverDTO(caregiver) instanceof CaregiverDTO);
    }

    @Test
    public void testEntityConvertor2(){
        Patient patient = new Patient();
        patient.setUser(new User());
        assertTrue(EntityConvertor.convertToPatientDTO(patient) instanceof PatientDTO);
    }

    @Test
    public void testEntityConvertor3(){
        assertTrue(EntityConvertor.convertToUserDTO(new User()) instanceof UserDTO);
    }

}
